from django.contrib.auth.models import User
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from reservation.models import Reservation
from reservation.serializers import ReservationSerializer

from .serializers import UserSerializer


class EmployeeViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer

    def list(self, request):
        data = User.objects.all()
        serializer = UserSerializer(data, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            User.objects.create_user(
                **serializer.validated_data,
            )
            return Response(
                serializer.validated_data,
                status=status.HTTP_201_CREATED
            )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )

    def update(self, request, pk=None):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            user = User.objects.get(pk=pk)
            data = serializer.validated_data
            user.first_name = data.get('first_name', user.first_name)
            user.last_name = data.get('last_name', user.last_name)
            user.username = data.get('username', user.username)
            user.email = data.get('email', user.email)
            user.set_password(data.get('password', user.password))
            user.save()
            return Response(
                serializer.validated_data,
                status=status.HTTP_200_OK)
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )

    @action(detail=True)
    def reservations(self, request, pk=None):
        data = Reservation.objects.filter(pk=pk)
        serializer = ReservationSerializer(data, many=True)
        return Response(serializer.data)
