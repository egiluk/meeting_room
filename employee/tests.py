from datetime import datetime, timedelta

from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient, APITestCase

from reservation.models import Reservation
from room.models import Room


class EmployeeTests(APITestCase):
    def setUp(self):
        self.user = User.objects.create(
            username="John",
            password="pass",
            email='a@gmail.com',
            irst_name='a',
            last_name='a'
        )
        User.objects.create(
            username="Steve",
            password="pass",
            email='s@gmail.com',
            first_name='s',
            last_name='s'
        )
        self.room = Room.objects.create(
            name='room1',
            max_people="20",
            area="20"
        )
        self.reservation = Reservation.objects.create(
            title='interview',
            date_from=datetime.now(),
            date_to=datetime.now() + timedelta(seconds=360),
            room=self.room, employee=self.user
        )

        token = Token.objects.get(user=self.user)
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

    def test_list_employee(self):
        response = self.client.get('/employee/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data[0]['first_name'], 'a')
        self.assertEqual('John', response.data[0].get('username'))

    def test_get_employee(self):
        response = self.client.get('/employee/1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['first_name'], 'a')
        response = self.client.get('/employee/2/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['first_name'], 's')
        response = self.client.get('/employee/3/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_create_employee(self):
        response = self.client.post('/employee/', {
            'username': 'Mark',
            'password': 'pass',
            'email': 'a@gmail.com',
            'first_name': 'm',
            'last_name': 'm'
        })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client.get('/employee/')
        self.assertEqual(len(response.data), 3)
        self.assertEqual(response.data[2]['first_name'], 'm')

    def test_update_employee(self):
        response = self.client.put('/employee/1/', {
            'first_name': 'd',
            'last_name': 'd',
            'password': 'pass',
            'username': 'Johny'
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('/employee/1/')
        self.assertEqual(response.data['first_name'], 'd')

    def test_delete_employee(self):
        response = self.client.get('/employee/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        response = self.client.delete('/employee/2/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        response = self.client.get('/employee/')
        self.assertEqual(len(response.data), 1)

    def test_employee_reservations(self):
        response = self.client.get('/employee/1/reservations/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.reservation.title, response.data[0]['title'])
