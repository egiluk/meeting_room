from django.contrib.auth.models import User
from django.db import models

from room.models import Room


class Reservation(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=20)
    date_from = models.DateTimeField(null=False)
    date_to = models.DateTimeField(null=False)
    room = models.ForeignKey(Room, on_delete=models.PROTECT)
    employee = models.ForeignKey(User, on_delete=models.PROTECT)
    canceled = models.BooleanField(default=False)

    class Meta:
        ordering = ['date_from']
        unique_together = ('date_from', 'date_to')
