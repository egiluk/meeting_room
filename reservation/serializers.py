from rest_framework import serializers, status
from rest_framework.response import Response

from .models import Reservation


class ReservationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reservation
        fields = ['id', 'title', 'date_from', 'date_to', 'room', 'employee']

        def validate(self, data):
            if (data['date_to'] - data['date_from']).seconds < 600:
                return Response(
                    'Minimum time for reservation 10 minutes',
                    status=status.HTTP_400_BAD_REQUEST
                )
            if Reservation.objects.filter(
                    date_from__range=(data['date_from'], data['date_to']),
                    canceled=False):
                return Response(
                    'Room already reserved for this time',
                    status=status.HTTP_400_BAD_REQUEST
                )
            return data
