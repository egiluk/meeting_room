from datetime import date

from django.shortcuts import get_object_or_404
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from .models import Reservation
from .serializers import ReservationSerializer


class ReservationViewSet(viewsets.ModelViewSet):
    queryset = Reservation.objects.all()
    serializer_class = ReservationSerializer

    def list(self, request):
        today = date.today()
        data = Reservation.objects.filter(date_from__gte=today, canceled=False)
        serializer = ReservationSerializer(data, many=True)
        return Response(serializer.data)

    def destroy(self, request, pk=None):
        queryset = Reservation.objects.all()
        data = get_object_or_404(queryset, pk=pk)
        data.canceled = True
        data.save()
        return Response(
            'Reservation canceled', status=status.HTTP_204_NO_CONTENT)

    @action(detail=False)
    def all(self, request):
        data = Reservation.objects.all()
        serializer = ReservationSerializer(data, many=True)
        return Response(serializer.data)
