from datetime import datetime, timedelta

from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient, APITestCase

from reservation.models import Reservation
from room.models import Room


class ReservationTests(APITestCase):
    def setUp(self):
        self.date_from = datetime.now()
        self.date_to = datetime.now() + timedelta(seconds=360)
        self.user = User.objects.create(
            username="John",
            password="pass",
            email='a@gmail.com',
            first_name='a',
            last_name='a'
        )
        self.room = Room.objects.create(
            name='room1',
            max_people="20",
            area="20"
        )
        self.reservation = Reservation.objects.create(
            title='interview',
            date_from=self.date_from,
            date_to=self.date_to,
            room=self.room,
            employee=self.user
        )
        self.reservation2 = Reservation.objects.create(
            title='interview2',
            date_from=self.date_from+timedelta(seconds=360),
            date_to=self.date_to + timedelta(seconds=720),
            room=self.room, employee=self.user
        )
        self.reservation3 = Reservation.objects.create(
            title='interview3',
            date_from=self.date_from - timedelta(days=1, minutes=60),
            date_to=self.date_to - timedelta(days=1),
            room=self.room, employee=self.user
        )

        token = Token.objects.get(user=self.user)
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

    def test_list_reservation(self):
        response = self.client.get('/reservation/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data[0]['title'],  self.reservation.title)

    def test_get_reservation(self):
        response = self.client.get('/reservation/1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['title'], self.reservation.title)
        response = self.client.get('/reservation/2/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['title'], self.reservation2.title)
        response = self.client.get('/reservation/4/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_create_reservation(self):
        response = self.client.post('/reservation/', {
            'title': 'interview4',
            'date_from': datetime.now() + timedelta(seconds=1000),
            'date_to': datetime.now() + timedelta(seconds=2000),
            'room': self.room.pk, 'employee': self.user.pk,
        })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client.get('/reservation/')
        self.assertEqual(len(response.data), 3)
        self.assertEqual(response.data[2]['title'], 'interview4')

    def test_update_reservation(self):
        response = self.client.put('/reservation/1/', {
            'title': 'interview4',
            'date_from': datetime.now() + timedelta(seconds=1000),
            'date_to': datetime.now() + timedelta(seconds=2000),
            'room': self.room.pk, 'employee': self.user.pk,
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('/reservation/1/')
        self.assertEqual(response.data['title'], 'interview4')

    def test_delete_reservation(self):
        response = self.client.get('/reservation/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        response = self.client.delete('/reservation/2/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_reservation_all(self):
        response = self.client.get('/reservation/all/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)
