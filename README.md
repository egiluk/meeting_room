## Create a virtual environment
python3 -m venv env

source env/bin/activate

## Install packages
pip install -r requirements.txt

## run migrations
./manage.py makemigrations

./manage.py migrate

## Create account
./manage.py createsuperuser

## Run server
./manage.py runserver

# Using Api
## get auth token
POST http://localhost:8000/api-token-auth/

body: { "username": "your created username", "password": "your created password"}

copy response token http://localhost:8000/api-token-auth/

**now in every request add HEADER - AUTHORIZATION: Token 'your token'** 

## Creating meeting_room
POST http://localhost:8000/room/ (add AUTHORIZATION HEADER) 

body: {'name': 'room1', 'max_people': '10', 'area': '10'}

## Creating reservation
POST http://localhost:8000/rezervation/ (add AUTHORIZATION HEADER)

body{ "title": "interview", 
      "date_from'": "2019-12-12 11:20:00", 
      "date_to": "2019-12-12 11:50:00",
      "room": 1, 
      "employee": 1
      }
## Creating employee
POST http://localhost:8000/employee/ (add AUTHORIZATION HEADER)

body:{enter body}

## Listing:
GET http://localhost:8000/employee/ (add AUTHORIZATION HEADER)

GET http://localhost:8000/reservation/ (add AUTHORIZATION HEADER)

GET http://localhost:8000/room/ (add AUTHORIZATION HEADER)

## Detailed listing:
GET http://localhost:8000/employee/'employee_id'/ (add AUTHORIZATION HEADER)

GET http://localhost:8000/reservation/'reservation_id'/ (add AUTHORIZATION HEADER)

GET http://localhost:8000/room/'user_id'/ (add AUTHORIZATION HEADER)

## Updating:
GET http://localhost:8000/employee/'employee_id'/ (add AUTHORIZATION HEADER)

GET http://localhost:8000/reservation/'reservation_id'/ (add AUTHORIZATION HEADER)

GET http://localhost:8000/room/user_id/ (add AUTHORIZATION HEADER)

body:{your updating data}

## Deleting
DELETE http://localhost:8000/employee/'employee_id'/ (add AUTHORIZATION HEADER)

DELETE http://localhost:8000/reservation/'reservation_id'/ (add AUTHORIZATION HEADER)

DELETE http://localhost:8000/room/user_id/ (add AUTHORIZATION HEADER)

## Get reservations by user
GET http://localhost:8000/employee/'employee_id'/reservations/

## Get all reservations
GET http://localhost:8000/reservation/all/


# Run tests

./manage.py test
 