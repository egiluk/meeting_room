from django.db import models


class Room(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=20)
    max_people = models.IntegerField(default=2)
    area = models.IntegerField(default=2)

    class Meta:
        ordering = ['created']
        unique_together = ('name', 'area')
