from rest_framework import viewsets
from rest_framework.response import Response

from .models import Room
from .serializers import RoomSerializer


class RoomViewSet(viewsets.ModelViewSet):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer

    def list(self, request):
        data = Room.objects.all()
        serializer = RoomSerializer(data, many=True)
        return Response(serializer.data)
