from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient, APITestCase

from room.models import Room


class RoomTests(APITestCase):
    def setUp(self):
        self.user = User.objects.create(
            username="John",
            password="pass",
            email='a@gmail.com',
            first_name='a',
            last_name='a'
        )
        self.room = Room.objects.create(
            name='room1',
            max_people="20",
            area="20"
        )
        self.room2 = Room.objects.create(
            name='room2',
            max_people="15",
            area="15"
        )

        token = Token.objects.get(user=self.user)
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

    def test_list_room(self):
        response = self.client.get('/room/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data[0]['name'], self.room.name)

    def test_get_room(self):
        response = self.client.get('/room/1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['name'], self.room.name)
        response = self.client.get('/room/2/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['name'], self.room2.name)
        response = self.client.get('/employee/3/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_create_room(self):
        response = self.client.post(
            '/room/', {'name': 'room3', 'max_people': '10', 'area': '10'})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client.get('/room/')
        self.assertEqual(len(response.data), 3)
        self.assertEqual(response.data[2]['name'], 'room3')

    def test_update_room(self):
        response = self.client.put(
            '/room/1/', {'name': 'room4', 'max_people': '20', 'area': '10'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('/room/1/')
        self.assertEqual(response.data['name'], 'room4')

    def test_delete_room(self):
        response = self.client.get('/room/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        response = self.client.delete('/room/2/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        response = self.client.get('/room/')
        self.assertEqual(len(response.data), 1)
