from django.urls import include, path
from rest_framework.authtoken import views
from rest_framework.routers import DefaultRouter

from employee.views import EmployeeViewSet
from reservation.views import ReservationViewSet
from room.views import RoomViewSet

router = DefaultRouter()
router.register(r'employee', EmployeeViewSet)
router.register(r'reservation', ReservationViewSet)
router.register(r'room', RoomViewSet)

# The API URLs are now determined automatically by the router.
urlpatterns = [
    path('', include(router.urls)),
    path('api-token-auth/', views.obtain_auth_token),
]
